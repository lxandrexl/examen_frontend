export interface Grado {
    id_grado?: number;
    desc_grado: string;
    nivel: string;
}