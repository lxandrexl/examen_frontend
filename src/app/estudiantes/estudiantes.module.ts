import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuscadorEstudiantesComponent } from './components/buscador/buscador-estudiantes.component';
import { EstudiantesService } from './services/estudiantes.service';
import { MaterialModule } from '../material.module';
import { ListadoEstudiantesComponent } from './components/listado/listado-estudiantes.component';
import { DialogEstudiantesComponent } from './components/dialog/dialog-estudiantes.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [BuscadorEstudiantesComponent, ListadoEstudiantesComponent, DialogEstudiantesComponent],
  exports: [BuscadorEstudiantesComponent],
  imports: [CommonModule, HttpClientModule, MaterialModule, FormsModule],
  providers: [EstudiantesService],
  entryComponents: [DialogEstudiantesComponent]
})
export class EstudiantesModule { }
