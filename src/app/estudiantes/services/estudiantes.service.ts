import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Grado } from '../interfaces/grado';
import { Persona } from '../interfaces/persona';

const baseUrl = 'http://127.0.0.1:3000/estudiantes';

@Injectable({
  providedIn: 'root'
})
export class EstudiantesService {
  filterEstudiante: Subject<string> = new Subject<string>();
  reloadListadoEstudiantes: Subject<Persona> = new Subject<Persona>();
  obs_filterEst: Observable<string> = this.filterEstudiante;
  obs_reloadListadoEst: Observable<Persona> = this.reloadListadoEstudiantes;

  constructor(private http: HttpClient) { }

  insertPersona(persona: Persona): Observable<Persona> {
    return this.http.post<Persona>(baseUrl, persona);
  }

  getPersonas(): Observable<Persona[]> {
    return this.http.get<Persona[]>(baseUrl);
  }

  getGrados(): Observable<Grado[]> {
    return this.http.get<Grado[]>(baseUrl + '/grados')
  }

  deletePersona(id: number): Observable<any> {
    return this.http.post(`${baseUrl}/delete`, { id });
  }
}
