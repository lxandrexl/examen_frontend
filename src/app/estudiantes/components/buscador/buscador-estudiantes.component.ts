import { Component, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogEstudiantesComponent } from '../dialog/dialog-estudiantes.component';
import { Persona } from "../../interfaces/persona";
import { EventEmitter } from '@angular/core';
import { EstudiantesService } from '../../services/estudiantes.service';

@Component({
  selector: 'app-buscador-estudiantes',
  templateUrl: './buscador-estudiantes.component.pug',
  styleUrls: ['./buscador-estudiantes.component.css']
})
export class BuscadorEstudiantesComponent implements OnInit {
  constructor(public dialog: MatDialog, private estudiantesService: EstudiantesService) { }

  ngOnInit() {
  }

  appyFilter(filterValue: string) {
    this.estudiantesService.filterEstudiante.next(filterValue);
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogEstudiantesComponent, { disableClose: true, width: '500px', panelClass: 'my-dialog' });

    dialogRef.afterClosed().subscribe((persona: Persona) => {
      if (persona === undefined) return;

      this.estudiantesService.reloadListadoEstudiantes.next(persona);
    })
  }
}
