import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEstudiantesComponent } from './dialog-estudiantes.component';

describe('DialogEstudiantesComponent', () => {
  let component: DialogEstudiantesComponent;
  let fixture: ComponentFixture<DialogEstudiantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEstudiantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEstudiantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
