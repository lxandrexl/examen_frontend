import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Persona } from '../../interfaces/persona';
import { Grado } from '../../interfaces/grado';
import { EstudiantesService } from '../../services/estudiantes.service';
import * as moment from 'moment';

@Component({
  selector: 'app-dialog-estudiantes',
  templateUrl: './dialog-estudiantes.component.pug',
  styleUrls: ['./dialog-estudiantes.component.css']
})
export class DialogEstudiantesComponent implements OnInit {
  miEdad: string = "año(s) mes(es)";
  estudiante: Persona = {
    nom_persona: '',
    ape_mate_pers: '',
    ape_pate_pers: '',
    fecha_naci: null,
    foto_ruta: '',
    id_grado: null
  };

  grados: Grado[] = [];

  constructor(
    private estudiantesService: EstudiantesService,
    public dialogRef: MatDialogRef<DialogEstudiantesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    this.estudiantesService.getGrados().subscribe((response) => {
      this.grados = response;
    }, err => console.log(err));
  }

  onClose(): void {
    this.dialogRef.close();
  }

  crear(): void {
    if (this.estudiante.nom_persona.trim().length <= 0 || this.estudiante.ape_mate_pers.trim().length <= 0 ||
      this.estudiante.ape_pate_pers.trim().length <= 0 || this.estudiante.id_grado == null ||
      this.estudiante.fecha_naci == null               || this.estudiante.foto_ruta.trim().length <= 0) {
      alert('Completa todo los campos');
      return;
    }

    this.estudiantesService.insertPersona(this.estudiante).subscribe((persona) => {
      this.dialogRef.close(persona);
    }, err => console.log(err));
  }

  calcularEdad(): void {
    if (this.estudiante.fecha_naci == null) return;

    let actual = moment();
    let input = moment(this.estudiante.fecha_naci);
    let anio = Math.round(actual.diff(input, 'months') / 12);
    let meses = actual.diff(input, 'months') % 12;

    this.miEdad = anio + ' año(s) ' + meses + ' mes(es)';
  }

  subirImagen(fileInput): void {
    if (fileInput.target.files && fileInput.target.files[0]) {

      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          this.estudiante.foto_ruta = e.target.result;
        };
      };

      reader.readAsDataURL(fileInput.target.files[0]);
    }

  }
}

