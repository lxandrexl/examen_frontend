import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Persona } from '../../interfaces/persona';
import { EstudiantesService } from '../../services/estudiantes.service';
import * as moment from 'moment';
import { Grado } from '../../interfaces/grado';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-listado-estudiantes',
  templateUrl: './listado-estudiantes.component.pug',
  styleUrls: ['./listado-estudiantes.component.css']
})
export class ListadoEstudiantesComponent implements OnInit {
  estudiantes: Persona[] = [];
  estudiantes_temp: Persona[] = [];
  grados: Grado[] = [];

  constructor(private estudiantesService: EstudiantesService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.listar();
    this.listenObservables();
  }

  listenObservables(): void {
    this.estudiantesService.obs_reloadListadoEst.subscribe((value: Persona) => this.agregarEstudiante(value));
    this.estudiantesService.obs_filterEst.subscribe((value: string) => this.filtrar(value));
  }

  agregarEstudiante(estudiante: Persona): void {
    this.estudiantes_temp.splice(0, 0, estudiante);
    this.estudiantes.splice(0, 0, estudiante);
    this.openSnackBar('Estudiante creado correctamente.');
  }

  openSnackBar(message: string): void {
    this._snackBar.open(message, 'Cerrar', {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    });
  }

  listar(): void {
    this.estudiantes = [];
    this.estudiantes_temp = [];
    this.estudiantesService.getPersonas().subscribe((personas: Persona[]) => {
      personas.forEach((persona: Persona) => {
        this.estudiantes.push(persona);
        this.estudiantes_temp.push(persona);
      })
    }, err => console.log(err));

    this.estudiantesService.getGrados().subscribe((grados: Grado[]) => {
      this.grados = grados;

    }, err => console.log(err));
  }

  darNombre(persona: Persona): string {
    return persona.nom_persona + ' ' + persona.ape_pate_pers;
  }

  filtrar(filtroEstudiante): void {
    let arrWords = filtroEstudiante.toLowerCase().split(' ');
    let status = false;
    this.estudiantes = [];

    arrWords.map(word => {
      if (word.trim().length <= 0) return;

      if (this.estudiantes.length > 0) {
        this.estudiantes.forEach(est =>
          arrWords.map(wordFiltered => { if (est.nom_persona.toLowerCase() == wordFiltered) status = true; })
        );

        this.estudiantes = this.estudiantes.filter(estudFilter => estudFilter.ape_pate_pers.toLowerCase().indexOf(word) > -1);
      }

      if (!status) this.estudiantes = this.estudiantes_temp.filter(x => x.nom_persona.toLowerCase().indexOf(word) > -1);
    })

    if (this.estudiantes.length <= 0)
      this.estudiantes = this.estudiantes_temp.filter(x => x.ape_pate_pers.toLowerCase().indexOf(filtroEstudiante) > -1);
  }

  mostrarEdad(fecha): string {
    if (fecha == null) return;

    let actual = moment();
    let input = moment(fecha);
    let anio = Math.round(actual.diff(input, 'months') / 12);
    let meses = actual.diff(input, 'months') % 12;

    return anio + ' año(s) ' + meses + ' mes(es)';
  }

  mostrarGrado(gradoId): string {
    let grados = this.grados.filter(grado => grado.id_grado === gradoId);

    if (grados.length <= 0) return;

    let grado: Grado = grados.shift();

    return grado.desc_grado + ' de ' + grado.nivel;
  }

  deleteEstudiante(persona: Persona): void {
    let action = confirm(`¿Seguro que desea eliminar al estudiante ${persona.nom_persona} ${persona.ape_pate_pers} ?`);

    if (!action) return;

    this.estudiantesService.deletePersona(persona.id_persona)
      .subscribe((response) => {
        alert(response.message);

        if (response.type == 'success') this.listar();

      }, err => console.log(err));
  }

}
