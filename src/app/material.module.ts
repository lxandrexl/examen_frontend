import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as Material from '@angular/material';
import { MAT_DATE_LOCALE } from '@angular/material';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Material.MatTabsModule,
    Material.MatGridListModule,
    Material.MatButtonModule,
    Material.MatInputModule,
    Material.MatIconModule,
    Material.MatCardModule,
    Material.MatDialogModule,
    Material.MatSelectModule,
    Material.MatFormFieldModule,
    Material.MatDatepickerModule,
    Material.MatNativeDateModule,
    Material.MatTableModule,
    Material.MatPaginatorModule,
    Material.MatSortModule,
    Material.MatCheckboxModule,
    Material.MatRadioModule,
    Material.MatSnackBarModule
  ],
  exports: [
    Material.MatTabsModule,
    Material.MatGridListModule,
    Material.MatButtonModule,
    Material.MatInputModule,
    Material.MatIconModule,
    Material.MatCardModule,
    Material.MatDialogModule,
    Material.MatSelectModule,
    Material.MatFormFieldModule,
    Material.MatDatepickerModule,
    Material.MatNativeDateModule,
    Material.MatTableModule,
    Material.MatPaginatorModule,
    Material.MatSortModule,
    Material.MatCheckboxModule,
    Material.MatRadioModule,
    Material.MatSnackBarModule
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'es-ES' }]
})
export class MaterialModule { }
