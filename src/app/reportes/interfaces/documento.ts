interface JsonExtraInfo {
    grado: string;
    nombres: string;
    monto_total: number;
}

export interface Documento {
    id_documento: number;
    nro_serie: string;
    nro_documento: string;
    tipo_documento: string;
    fecha_emision: Date;
    jsonb_info: JsonExtraInfo;
}