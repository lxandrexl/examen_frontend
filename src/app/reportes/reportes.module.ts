import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportesComponent } from './components/reportes.component';
import { MaterialModule } from '../material.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReportesService } from './services/reportes.service';
import { ListadoReportesComponent } from './components/listado-reportes/listado-reportes.component';
import { DialogReportePdfComponent } from './components/dialog-reporte-pdf/dialog-reporte-pdf.component';

@NgModule({
  declarations: [ReportesComponent, ListadoReportesComponent, DialogReportePdfComponent],
  exports: [ReportesComponent],
  imports: [CommonModule, MaterialModule, HttpClientModule, FormsModule],
  providers: [ReportesService],
  entryComponents: [DialogReportePdfComponent]
})
export class ReportesModule { }
