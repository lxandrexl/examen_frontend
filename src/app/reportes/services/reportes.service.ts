import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Movimiento } from 'src/app/pagos/interfaces/Movimiento';
import { Documento } from '../interfaces/documento';

const baseUrl = 'http://127.0.0.1:3000/reportes';

@Injectable({
  providedIn: 'root'
})
export class ReportesService {

  reloadTable: Subject<boolean> = new Subject<boolean>();
  obs_reloadTable: Observable<boolean> = this.reloadTable;

  constructor(private http: HttpClient) { }

  getReportes(): Observable<Documento[]> {
    return this.http.get<Documento[]>(baseUrl);
  }

  getItemsReporte(id): Observable<Movimiento[]> {
    return this.http.post<Movimiento[]>(`${baseUrl}/buscar-items`, { id });
  }
}
