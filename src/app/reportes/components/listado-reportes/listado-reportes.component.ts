import { Component, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { Documento } from '../../interfaces/documento';
import { ReportesService } from '../../services/reportes.service';
import * as moment from 'moment';
import { Movimiento } from 'src/app/pagos/interfaces/Movimiento';
import { MakePDF } from '../../utils/make-pdf';
import { DialogReportePdfComponent } from '../dialog-reporte-pdf/dialog-reporte-pdf.component';

@Component({
  selector: 'app-listado-reportes',
  templateUrl: './listado-reportes.component.pug',
  styleUrls: ['./listado-reportes.component.css']
})
export class ListadoReportesComponent implements OnInit {
  displayedColumns: string[] = ["serie", "fecha_reg", "estudiante", "monto", "pdf"];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  reportes: Documento[] = [];
  pdfMaker: MakePDF = new MakePDF();
  itemsReportes: Movimiento[] = [];

  constructor(
    private reporteService: ReportesService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.getReportes();
    this.listenObsChanges();
  }

  listenObsChanges() {
    this.reporteService.obs_reloadTable.subscribe((value: boolean) => this.getReportes());
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.listenReloadReporte && !changes.listenReloadReporte.firstChange) this.getReportes();
  }

  getReportes(): void {
    this.reportes = [];
    this.reporteService.getReportes().subscribe((reportes: Documento[]) => {
      reportes.map((reporte: Documento) => this.reportes.push(reporte));
      this.dataSource = new MatTableDataSource(this.reportes);
      this.dataSource.paginator = this.paginator;
    }, err => console.log(err));
  }

  getFecha(date): string {
    return moment(date).format('YYYY-MM-DD');
  }

  openDialogPDF(reporte: Documento): void {
    const idReporte = reporte.id_documento;
    this.itemsReportes = [];

    this.reporteService.getItemsReporte(idReporte).subscribe((items: Movimiento[]) => {
      items.map((value: Movimiento) => this.itemsReportes.push(value));
      this.configPDF(reporte);
    }, err => console.log(err));
  }

  configPDF(reporte: Documento): void {
    const data = { documento: reporte, movimientos: this.itemsReportes };

    const pdfDocGenerator = this.pdfMaker.openPdf(data);

    pdfDocGenerator.getDataUrl((dataUrl) => {
      console.log(dataUrl)
      const dialogRef = this.dialog.open(DialogReportePdfComponent,
        { data: { pdf: dataUrl }, disableClose: true, width: '800px', height: '95vh' });

      dialogRef.afterClosed().subscribe((data) => {
        if (data === undefined) return;

      })
    });
  }

}
