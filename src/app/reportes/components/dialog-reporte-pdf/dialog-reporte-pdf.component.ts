import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-reporte-pdf',
  templateUrl: './dialog-reporte-pdf.component.pug',
  styleUrls: ['./dialog-reporte-pdf.component.css']
})
export class DialogReportePdfComponent implements OnInit {
  blobFile: string = '';

  constructor(
    public dialogRef: MatDialogRef<DialogReportePdfComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.createIframe();
  }

  onClose(): void {
    this.dialogRef.close();
  }

  createIframe() {
    const targetElement = document.querySelector('#iframeContainer');
    const iframe = document.createElement('iframe');
    iframe.src = this.data.pdf;
    iframe.width = "100%";
    iframe.height = "100%";
    iframe.style.border = "0";
    targetElement.appendChild(iframe);
  }

}
