import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogReportePdfComponent } from './dialog-reporte-pdf.component';

describe('DialogReportePdfComponent', () => {
  let component: DialogReportePdfComponent;
  let fixture: ComponentFixture<DialogReportePdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogReportePdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogReportePdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
