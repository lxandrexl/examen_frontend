import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from 'pdfmake/build/vfs_fonts';
import * as moment from 'moment';
import { Movimiento } from 'src/app/pagos/interfaces/Movimiento';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export class MakePDF {


    constructor() { }

    openPdf(data): any {
        let serie = data.documento.nro_serie + '-' + data.documento.nro_documento;
        let nombres = data.documento.jsonb_info.nombres;
        let grado = data.documento.jsonb_info.grado;
        let fecha_reg = moment(data.documento.fecha_emision).format('YYYY-MM-DD');
        let bodyTable: any = [
            [
                { text: 'Ctn', fontSize: 7, alignment: 'center', margin: [0, 0, 0, 0] },
                { text: 'Descripción', fontSize: 7, alignment: 'center', margin: [0, 0, 0, 0] },
                { text: 'Monto', fontSize: 7, alignment: 'center', margin: [0, 0, 0, 0] },
                { text: 'Dscto', fontSize: 7, alignment: 'center', margin: [0, 0, 0, 0] },
                { text: 'Monto Final', fontSize: 7, alignment: 'center', margin: [0, 0, 0, 0] }
            ]
        ];

        data.movimientos.map((mov: Movimiento, index: number) => {
            let row = [
                { text: index + 1, fontSize: 7, alignment: 'center' },
                { text: mov.__pension, fontSize: 7, alignment: 'center' },
                { text: `S/.${mov.monto}`, fontSize: 7, alignment: 'center' },
                { text: '0', fontSize: 7, alignment: 'center' },
                { text: `S/.${mov.monto}`, fontSize: 7, alignment: 'center' }
            ];
            bodyTable.push(row);
        });

        bodyTable.push([
            { text: 'Total', colSpan: 4, fontSize: 7, bold: true, alignment: 'center' },
            {}, {}, {},
            { text: `S/.${data.documento.jsonb_info.monto_total}`, fontSize: 7, bold: true, alignment: 'center' }
        ]);

        const documentDefinition = {
            pageMargins: [5, 20, 5, 20],
            pageOrientation: 'portrait',
            pageSize: { width: 180, height: 300 },
            content: [
                { text: 'Boleta de venta Electrónica', fontSize: 10, alignment: 'center', bold: true, margin: [0, 0, 0, 5] },
                { text: serie, fontSize: 4, alignment: 'center', margin: [0, 5, 0, 3] },
                { text: nombres, fontSize: 4, alignment: 'center', margin: [0, 3, 0, 3] },
                { text: `Grado:  ${grado}`, fontSize: 4, alignment: 'center', margin: [0, 3, 0, 3] },
                { text: `Fecha de Emisión:  ${fecha_reg}`, fontSize: 4, alignment: 'center', margin: [0, 3, 0, 3] },
                { text: 'Moneda: SOLES', fontSize: 4, alignment: 'center', margin: [0, 3, 0, 5] },
                { text: '-----------------------------------------------------------', alignment: 'center', fontSize: 4, margin: [0, 0, 0, 5]  },
                {
                    table: {
                        // headers are automatically repeated if the table spans over multiple pages
                        // you can declare how many rows should be treated as headers
                        headerRows: 1,
                        body: bodyTable
                    }
                },
                { text: '-----------------------------------------------------------', alignment: 'center', fontSize: 4, margin: [0, 5, 0, 0]  },
            ]
        };

        return pdfMake.createPdf(documentDefinition);
    }

}