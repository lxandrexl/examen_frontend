import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagosComponent } from './components/pagos.component';
import { PagosService } from './services/pagos.service';
import { MaterialModule } from '../material.module';
import { ListadoPagosComponent } from './components/listado-pagos/listado-pagos.component';
import { DialogPagosComponent } from './components/dialog-pagos/dialog-pagos.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DialogPagosMovimientosComponent } from './components/dialog-pagos-movimientos/dialog-pagos-movimientos.component';

@NgModule({
  declarations: [PagosComponent, ListadoPagosComponent, DialogPagosComponent, DialogPagosMovimientosComponent],
  exports: [PagosComponent],
  imports: [CommonModule, MaterialModule, HttpClientModule, FormsModule],
  providers: [PagosService],
  entryComponents: [DialogPagosComponent, DialogPagosMovimientosComponent]
})
export class PagosModule { }
