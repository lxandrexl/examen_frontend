import { Movimiento } from './Movimiento';

export interface CheckBox {
    index: number;
    checked: boolean;
    disabled: boolean;
    pago: Movimiento;
  }