export interface Movimiento {
    id_movimiento?: number;
    tipo_movimiento: string;
    monto: number;
    estado: string;
    fecha_pago: Date;
    id_persona: number;
    id_detalle_cronograma: number;
    __pension?: string;
    __fecha_venci?: Date;
}