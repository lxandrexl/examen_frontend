import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { Persona } from 'src/app/estudiantes/interfaces/persona';
import { EstudiantesService } from 'src/app/estudiantes/services/estudiantes.service';
import { ReportesService } from 'src/app/reportes/services/reportes.service';
import { CheckBox } from '../../interfaces/CheckBox';
import { Movimiento } from '../../interfaces/Movimiento';
import { PagosService } from '../../services/pagos.service';
import { DialogPagosMovimientosComponent, ListadoData } from '../dialog-pagos-movimientos/dialog-pagos-movimientos.component';

const PAGADO = 'PAGADO';
const POR_PAGAR = 'POR PAGAR';
const ANULADO = 'ANULADO';
const VENCIDO = 'VENCIDO';

@Component({
  selector: 'app-listado-pagos',
  templateUrl: './listado-pagos.component.pug',
  styleUrls: ['./listado-pagos.component.css']
})
export class ListadoPagosComponent implements OnInit {
  displayedColumns: string[] = ["status", "pension", "monto", "estado"];
  dataSource: MatTableDataSource<any>;
  pagos: Movimiento[] = [];
  btnPagar: boolean = true;
  btnAnular: boolean = true;
  movimientos: Movimiento[] = [];
  checkBoxGroup: CheckBox[] = [];
  rowsChecked: CheckBox[] = [];
  estudiante_cached: Persona = null;
  estudianteSeleccionado: Persona = null;

  constructor(
    private pagosService: PagosService,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private reporteService: ReportesService,
    private estudiantesService: EstudiantesService
  ) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.pagos);
    this.loadMovimientos();
    this.listenObservables();
  }

  listenObservables(): void {
    this.pagosService.obs_filtrarPersonaMov.subscribe((value: Persona) => {
      this.estudianteSeleccionado = value;
      this.getPagos();
      this.loadMovimientos();
    });
  }

  loadMovimientos(isLoaded: boolean = true): void {
    if (isLoaded) {
      this.pagosService.getMovimientos().subscribe((movimientos: Movimiento[]) => {
        this.movimientos = [];
        movimientos.map((movimiento: Movimiento, index: number) => this.movimientos.push(movimiento));
      }, err => console.log(err));
    } else {
      this.pagosService.getMovimientos().subscribe((movimientos: Movimiento[]) => {
        this.pagos = movimientos.filter((mov: Movimiento) => mov.id_persona === this.estudiante_cached.id_persona);
        this.dataSource = new MatTableDataSource(this.pagos);
        this.setCheckBoxGroup();
      }, err => console.log(err));
    }
  }

  getPagos(isNew: boolean = true): void {
    if (isNew) {
      //Filtro los movimientos del estudiante seleccionado en el componente "DialogPagos"(@Input())
      this.pagos = this.movimientos.filter((mov: Movimiento) => mov.id_persona === this.estudianteSeleccionado.id_persona);
      this.dataSource = new MatTableDataSource(this.pagos);
      this.estudiante_cached = this.estudianteSeleccionado;
      this.setCheckBoxGroup();
    } else {
      this.loadMovimientos(false);
    }
  }

  setCheckBoxGroup(): void {
    this.checkBoxGroup = []; // Limpio el array que tenia data antigua
    this.pagos.map((value: Movimiento, index: number) => this.checkBoxGroup.push({ index, checked: false, disabled: true, pago: value }));

    // Habilito los checkbox desde la primera fila en estado ¨POR PAGAR"
    if (this.checkBoxGroup.length > 0) {
      let por_pagar_arr: CheckBox[] = this.checkBoxGroup.filter((value: CheckBox) => value.pago.estado == POR_PAGAR).slice();
      let por_pagar: CheckBox = (por_pagar_arr.length > 0) ? por_pagar_arr[0] : null;

      if (por_pagar != null)
        this.checkBoxGroup.map((value: CheckBox) => (value.index <= por_pagar.index) ? value.disabled = false : value.disabled = true);
      else
        this.checkBoxGroup.map((value: CheckBox) => value.disabled = false);
    }
  }

  onChangeCheckBox(index: number, event: any): void {
    // Verifico si estoy en la ultima fila del tablero
    if ((index + 1) === this.pagos.length) {
      if (event.checked) this.checkBoxGroup[index].checked = event.checked;
      else this.checkBoxGroup[index].checked = event.checked;
      this.checkActionButtons();
      return;
    }
    // Verifico la acción a los "check-button" del tablero
    switch (event.checked) {
      case true:
        this.checkBoxGroup[index + 1].disabled = !event.checked;;
        this.checkBoxGroup[index].checked = event.checked;
        this.checkActionButtons();
        break;
      case false:
        this.checkBoxGroup[index].checked = event.checked;
        // Deshabilito los checks que sobrepasan la fila actual (solo si esta en estado "POR PAGAR")
        if (this.checkBoxGroup[index].pago.estado == POR_PAGAR) {
          this.checkBoxGroup[index + 1].disabled = !event.checked;
          this.checkBoxGroup.forEach((cb: CheckBox) => {
            if (cb.index > index) {
              cb.checked = false;
              cb.disabled = true;
            }
          });
        }
        this.checkActionButtons();
        break;
    }
  }

  checkActionButtons(): void {
    let anularBtn: boolean = true;
    let pagarBtn: boolean = true;
    let findAnulado: boolean = false;

    this.rowsChecked = this.checkBoxGroup.filter((value: CheckBox) => value.checked == true);

    this.rowsChecked.forEach((value: CheckBox) => {
      let pago: Movimiento = value.pago;
      switch (pago.estado) {
        case POR_PAGAR: // Se activa boton pagar
          pagarBtn = false;
          break;
        case PAGADO: // Se activa boton anular 
          anularBtn = false;
          break;
        case ANULADO: // Encontro un movimiento en estado Anulado
          findAnulado = true;
          break;
      }
    });

    // Valido los botones del tablero (Anular, Pagar)
    if (!anularBtn && pagarBtn && !findAnulado) this.btnAnular = false;
    else if (!pagarBtn && anularBtn && !findAnulado) this.btnPagar = false;
    else { this.btnAnular = true; this.btnPagar = true; }
  }

  openDialog(type: string): void {
    let data: ListadoData = { type, rows: this.rowsChecked };

    const dialogRef = this.dialog.open(DialogPagosMovimientosComponent,
      { data, disableClose: true, width: '1000px', panelClass: 'my-dialog' });

    dialogRef.afterClosed().subscribe((data) => {
      if (data === undefined) return;

      this.reporteService.reloadTable.next(true);
      this.btnAnular = true;
      this.btnPagar = true;
      this.getPagos(false);
      this.openSnackBar(data.type == 'pago' ? 'Pago(s) realizado(s) correctamente!' : 'Anulacion(es) realizada(s) correctamente!');
    })
  }

  openSnackBar(message: string): void {
    this._snackBar.open(message, 'Cerrar', {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top'
    });
  }

}
