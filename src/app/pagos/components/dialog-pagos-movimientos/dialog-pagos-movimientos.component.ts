import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MatPaginator, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import * as moment from 'moment';
import { CheckBox } from '../../interfaces/CheckBox';
import { Movimiento } from '../../interfaces/Movimiento';
import { PagosService } from '../../services/pagos.service';

moment.locale('es');

export interface ListadoData {
  type: string;
  rows: CheckBox[];
}

@Component({
  selector: 'app-dialog-pagos-movimientos',
  templateUrl: './dialog-pagos-movimientos.component.pug',
  styleUrls: ['./dialog-pagos-movimientos.component.css']
})
export class DialogPagosMovimientosComponent implements OnInit {
  displayedColumns: string[] = ["pension", "fecha_venc", "monto"];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  btnAction: string = '';
  movimientos: Movimiento[] = [];

  constructor(
    private pagoService: PagosService,
    public dialogRef: MatDialogRef<DialogPagosMovimientosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ListadoData
  ) { }

  ngOnInit() {
    this.btnAction = this.data.type;
    this.data.rows.map((value: CheckBox) => this.movimientos.push(value.pago));
    this.dataSource = new MatTableDataSource(this.movimientos);
    this.dataSource.paginator = this.paginator;
  }

  setFechaVenc(date) {
    return moment(date).format('YYYY-MM-DD');
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSavePagar(): void {
    this.pagoService.setPagoMovimientos(this.movimientos).subscribe((response) => {
      this.dialogRef.close({ type: 'pago' });
    }, err => console.log(err));
  }

  onSaveAnular(): void {
    this.pagoService.setAnulacionMovimientos(this.movimientos).subscribe((response) => {
      this.dialogRef.close({ type: 'anulado' });
    }, err => console.log(err));
  }

}
