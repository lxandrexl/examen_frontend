import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPagosMovimientosComponent } from './dialog-pagos-movimientos.component';

describe('DialogPagosMovimientosComponent', () => {
  let component: DialogPagosMovimientosComponent;
  let fixture: ComponentFixture<DialogPagosMovimientosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogPagosMovimientosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPagosMovimientosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
