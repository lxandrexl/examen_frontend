import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Persona } from 'src/app/estudiantes/interfaces/persona';
import { PagosService } from '../services/pagos.service';
import { DialogPagosComponent } from './dialog-pagos/dialog-pagos.component';

@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.component.pug',
  styleUrls: ['./pagos.component.css']
})
export class PagosComponent implements OnInit {
  constructor(public dialog: MatDialog, private pagosService: PagosService) { }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogPagosComponent, { disableClose: true, width: '1000px', panelClass: 'my-dialog' });

    dialogRef.afterClosed().subscribe((persona: Persona) => {
      if (persona === undefined) return;

      this.pagosService.filtrarPersonaMov.next(persona);
    })
  }
}
