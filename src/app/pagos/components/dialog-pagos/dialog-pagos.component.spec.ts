import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPagosComponent } from './dialog-pagos.component';

describe('DialogPagosComponent', () => {
  let component: DialogPagosComponent;
  let fixture: ComponentFixture<DialogPagosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogPagosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPagosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
