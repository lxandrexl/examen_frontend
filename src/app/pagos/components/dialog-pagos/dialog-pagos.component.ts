import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MatPaginator, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { Grado } from 'src/app/estudiantes/interfaces/grado';
import { Persona } from 'src/app/estudiantes/interfaces/persona';
import { EstudiantesService } from 'src/app/estudiantes/services/estudiantes.service';

@Component({
  selector: 'app-dialog-pagos',
  templateUrl: './dialog-pagos.component.pug',
  styleUrls: ['./dialog-pagos.component.css']
})
export class DialogPagosComponent implements OnInit {
  displayedColumns: string[] = ["id", "nombre", "grado", "estado"];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  estudiantes: Persona[] = [];
  grados: Grado[] = [];
  estudianteSelected: Persona;

  constructor(
    private estudiantesService: EstudiantesService,
    public dialogRef: MatDialogRef<DialogPagosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.getGrados();
    this.getEstudiantes();
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) this.dataSource.paginator.firstPage();
  }

  getGrados(): void {
    this.estudiantesService.getGrados().subscribe((grados: Grado[]) => {
      grados.map(grado => this.grados.push(grado));
    }, err => console.log(err));
  }

  getEstudiantes(): void {
    this.estudiantesService.getPersonas().subscribe((personas: Persona[]) => {
      personas.map(persona => this.estudiantes.push(persona));
      this.dataSource = new MatTableDataSource(this.estudiantes);
      this.dataSource.paginator = this.paginator;
    }, err => console.log(err));
  }

  getRow(estudiante: Persona): void {
    this.estudianteSelected = estudiante;
  }

  mostrarGrado(gradoId): string {
    let grados = this.grados.filter(grado => grado.id_grado === gradoId);

    if (grados.length <= 0) return;

    let grado: Grado = grados.shift();

    return grado.desc_grado + ' de ' + grado.nivel;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSave(): void {
    this.dialogRef.close(this.estudianteSelected);
  }

}
