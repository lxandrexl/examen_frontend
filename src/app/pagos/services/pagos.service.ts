import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Persona } from 'src/app/estudiantes/interfaces/persona';
import { Movimiento } from '../interfaces/Movimiento';

const baseUrl = 'http://127.0.0.1:3000/pagos';

@Injectable({
  providedIn: 'root'
})
export class PagosService {
  filtrarPersonaMov: Subject<Persona> = new Subject<Persona>();
  obs_filtrarPersonaMov: Observable<Persona> = this.filtrarPersonaMov;

  constructor(private http: HttpClient) { }

  getMovimientos(): Observable<Movimiento[]> {
    return this.http.get<Movimiento[]>(`${baseUrl}/movimientos`);
  }

  setPagoMovimientos(movimientos: Movimiento[]): Observable<any> {
    return this.http.post(`${baseUrl}/pago`, movimientos);
  }

  setAnulacionMovimientos(movimientos: Movimiento[]): Observable<any> {
    return this.http.post(`${baseUrl}/anulacion`, movimientos);
  }
}
